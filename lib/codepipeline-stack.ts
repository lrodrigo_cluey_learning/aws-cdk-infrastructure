import { Stack, StackProps } from 'aws-cdk-lib';
import { ReadWriteType, Trail } from 'aws-cdk-lib/aws-cloudtrail';
import { BuildSpec, PipelineProject } from 'aws-cdk-lib/aws-codebuild';
import { Artifact, Pipeline } from 'aws-cdk-lib/aws-codepipeline';
import { CodeBuildAction, ManualApprovalAction, S3SourceAction, S3Trigger } from 'aws-cdk-lib/aws-codepipeline-actions';
import { Bucket } from 'aws-cdk-lib/aws-s3';
import { Construct } from 'constructs';

const PipelineConfig = {
  name: 'MyFirstCDKPipeline',
  source: {
    bucketName: 'argentinos-bondi',
    key: 'someKey.zip',
  },
};

export class CodePipelineStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const sourceBucket = Bucket.fromBucketName(this, 'Bucket', PipelineConfig.source.bucketName);

    const sourceOutput = new Artifact();
    const key = PipelineConfig.source.key;
    const trail = new Trail(this, 'CloudTrail');

    trail.addS3EventSelector([{
      bucket: sourceBucket,
      objectPrefix: key,
    }], {
      readWriteType: ReadWriteType.WRITE_ONLY,
    });

    const sourceAction = new S3SourceAction({
      actionName: 'S3Source',
      bucketKey: key,
      bucket: sourceBucket,
      output: sourceOutput,
      trigger: S3Trigger.NONE, // S3Trigger.EVENTS, // default: S3Trigger.POLL
    });

    const stagingCodeBuildAction = new CodeBuildAction({
      actionName: 'DeployStaging',
      input: sourceOutput,
      project: new PipelineProject(this, 'MyPipelineProject', {
        buildSpec: BuildSpec.fromObject({
          version: '0.2',
          phases: {
            build: {
              commands: [
                'echo Test',
                'ls -lsha',
              ],
            },
          },
        }),
      }),
      // executeBatchBuild: true,
    });

    /*
    // Don't create Customer Master Keys
    const pipeline = new Pipeline(this, 'MyFirstPipeline', {
      pipelineName: PipelineConfig.name,
      crossAccountKeys: false,
      stages: [
        {
          stageName: 'Source',
          actions: [sourceAction],
        },
        {
          stageName: 'Staging',
          actions: [stagingCodeBuildAction],
        },
      ],
    });
    */

    const pipeline = new Pipeline(this, 'MyFirstPipeline', {
      pipelineName: PipelineConfig.name,
      crossAccountKeys: false,
    });

    const source = pipeline.addStage({
      stageName: 'Source',
      actions: [sourceAction],
    });

    const staging = pipeline.addStage({
      stageName: 'Staging',
      actions: [stagingCodeBuildAction],
    });

    const manualApprovalAction = new ManualApprovalAction({
      actionName: 'Approve',
      // notificationTopic: new sns.Topic(this, 'Topic'), // optional
      // notifyEmails: [
      //  'some_email@example.com',
      // ], // optional
      additionalInformation: 'additional info', // optional
    });

    const approveStage = pipeline.addStage({ stageName: 'Approve' });
    approveStage.addAction(manualApprovalAction);

    const production = pipeline.addStage({
      stageName: 'Production',
      actions: [stagingCodeBuildAction],
    });
  }
}
